const axios = require('axios');
const cachios = require('cachios');
const componentId = process.env['componentId'] || "4qq900km9kw4";
const axiosInstance = axios.create({
    baseURL: 'http://ip-api.com/json/',
    timeout: 10000,
    headers: {
        'Content-Type':'application/json'
    }
});
const ipAPI = cachios.create(axiosInstance,{
    ttl : 60,
});

const statusPage = axios.create({
    baseURL: 'https://api.statuspage.io/v1/pages/dkx5l24gps3r/',
    timeout: 10000,
    headers: {
        'Authorization':'OAuth 5a5a159e-3ae4-4010-9c3e-4da2955cbf64',
        'Content-Type':'application/json'
    }
  });
  const monitorAPI = axios.create({
    baseURL: 'https://monitor.atfirstbyte.net/api/',
    timeout: 10000,
    headers: {
        'Authorization':'OAuth 5a5a159e-3ae4-4010-9c3e-4da2955cbf64',
        'Content-Type':'application/json'
    }
  });

  
module.exports.getIpAddressInfo = async(ip) => {
        return await ipAPI.get(ip);
}

module.exports.updateComponentStatus = async (status) => {
	let requestBody = {
	  "component": {
	    "status": status
	  }
	}
	return await statusPage.patch('components/'+componentId,requestBody);
}

module.exports.updateMonitorStatus = async (client,status) => {
	let requestBody = {
	  	ipAddress: client.handshake.address,
	  	socketId: client.id
	}
	return await monitorAPI.post('speedtest/'+status,requestBody)
}