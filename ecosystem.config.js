module.exports = {
  apps : [{
    script: 'index.js',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      NET_NAME: 'TOT',
      PORT:'2030'
    },
    env_tot: {
      NODE_ENV: 'tot',
      NET_NAME: 'TOT',
      USE_HTTPS : 'true',
      DOMAIN : 'monitor.atfirstbyte.net',
      PORT:'2030'
    },
    env_cat: {
      NODE_ENV: 'tot',
      NET_NAME: 'CAT',
      USE_HTTPS : 'true',
      DOMAIN : 'monitor.atfirstbyte.net',
      PORT:'2032'
    }
  }],

  deploy : {
    tot : {
      user : 'root',
      host : 'atfirstbyte.net',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:mycools/netmonitorserv.git',
      path : '/home/afbmon/netmonitor_tot',
      "ssh_options": "StrictHostKeyChecking=no",
      'pre-deploy' : 'mkdir -p /home/afbmon/netmonitor_tot',
      'post-deploy' : 'npm install && pm2 startOrRestart ecosystem.config.js --env tot'
    },
    cat : {
      user : 'root',
      host : 'atfirstbyte.net',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:mycools/netmonitorserv.git',
      path : '/home/afbmon/netmonitor_cat',
      "ssh_options": "StrictHostKeyChecking=no",
      'pre-deploy' : 'mkdir -p /home/afbmon/netmonitor_cat',
      'post-deploy' : 'npm install && pm2 startOrRestart ecosystem.config.js --env cat'
    }
  }
};
