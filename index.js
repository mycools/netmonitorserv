/* SETTINGS */
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const USE_HTTPS = process.env['USE_HTTPS'] || "false";
const CERT_PATH = process.env['CERT_PATH'] || "/usr/local/directadmin/data/users/afbmon/domains/";
const DOMAIN = process.env['DOMAIN'] || "localhost";
const NET_NAME = process.env['NET_NAME'] || "TOT";

const PORT = process.env['PORT'] || 2030;
const fs = require('fs');
const app = require('express')();
const axios = require('axios');
const func = require('./function')
const mt = require('@pm2/io')
var pm2 = require('pm2');
if(USE_HTTPS=='true'){
    const privateKey = fs.readFileSync(CERT_PATH + DOMAIN + '.key').toString();
    const certificate = fs.readFileSync(CERT_PATH + DOMAIN + '.cert.combined').toString();
    const ca = fs.readFileSync(CERT_PATH + DOMAIN + '.cacert').toString();
    var server = require('https').Server({
        key: privateKey,
        cert: certificate,
        ca: ca,
        requestCert: false,
        rejectUnauthorized: false
    },app);
}else{
    var server = require('http').Server(app);
}
const io = require('socket.io')(server);
var clientInfo = {
    'ISP' : '-',
    'CLIENT_STATUS' : 'Offline',
    'CLIENT_IP' : '-'
};

mt.metric({
    name      : 'NETWORK',
    value   : function() {
        return NET_NAME + ":" + PORT;
    }
})
const metricISP = mt.metric({
    name    : 'ISP',
    value   : function() {
        return clientInfo.ISP;
    }
})
const metricClientStatus = mt.metric({
    name    : 'NETWORK STATUS',
    value   : function() {
        return clientInfo.CLIENT_STATUS;
    }
})
const metricClientIp = mt.metric({
    name    : 'NETWORK IP',
    value   : function() {
        return clientInfo.CLIENT_IP;
    }
})

app.get('*', async(req, res) => {
    let ipinfo = {};
    try{
        
        if(req.connection.remoteAddress != "127.0.0.1"){
            ipinfo = await func.getIpAddressInfo(req.connection.remoteAddress);
        }else{
            ipinfo = await func.getIpAddressInfo("1.10.143.55");
        }
    }catch(err){
        console.error(err);
        process.exit(2);
    }
    
    if(ipinfo.data != undefined){
        
        res.json({
            'status' : 'success',
            'ipaddress' : req.connection.remoteAddress,
            'countryCode' : ipinfo.data['countryCode'],
            'country' : ipinfo.data['country'],
            'isp' : ipinfo.data['isp'],
            'org' : ipinfo.data['org'],
            'as' : ipinfo.data['as'],
            'query' : ipinfo.data['query'],
           
        });
    }else{
        res.json({
            'status' : 'success',
            'ipaddress' : req.connection.remoteAddress,
            'ipinfo' : {}
        });
    }
    
});
io.on('connection', async (client) => {
    let ipinfo = {};
    try{
        
        if(client.handshake.address != "127.0.0.1"){
            ipinfo = await func.getIpAddressInfo(client.handshake.address);
        }else{
            ipinfo = await func.getIpAddressInfo("1.10.143.55");
        }
    }catch(err){
        console.error(err);
        process.exit(2);
    }

    clientInfo.ISP = ipinfo.data['isp'];
    clientInfo.CLIENT_STATUS = 'Online';
    clientInfo.CLIENT_IP = ipinfo.data['query'];
    console.log('user connected',client.id,'via',client.handshake.address)
    func.updateMonitorStatus(client,'connect');
    func.updateComponentStatus('operational')
    client.on('disconnect', () => {
        console.log('user disconnected',client.id,'via',client.handshake.address)
        func.updateMonitorStatus(client,'disconnect');
        func.updateComponentStatus('major_outage')
        clientInfo.CLIENT_STATUS = 'Offline';
    })
	client.on('ping', function() {
		console.log(data);
		socket.emit('pong');
	});
});

if(USE_HTTPS=="true"){
    console.info("SERVER IS ONLINE ON : https://"+DOMAIN+":"+PORT)
}else{
    console.info("SERVER IS ONLINE ON : http://"+DOMAIN+":"+PORT)
}
server.listen(PORT);


// pm2.connect(function(err) {
//     if (err) {
//       console.error(err);
//       return;
//       //process.exit(2);
//     }
//     // pm2.sendDataToProcessId({
//     //     'options' : {
//     //         'name' : 'TOT'
//     //     }
//     // });
//     //pm2.reloadLogs();
//     console.info("SERVER IS CONNECTED WITH PM2")

// });